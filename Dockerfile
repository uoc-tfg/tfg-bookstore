FROM maven:3.8.1-openjdk-17-slim as builder
WORKDIR /workspace
COPY src src
COPY pom.xml .
RUN mvn clean package -Dmaven.test.skip=true

FROM openjdk:17-jdk-slim
ENV SPRING_DEVTOOLS_RESTART_ENABLED true
WORKDIR /app
RUN mkdir uploads
RUN chmod 777 uploads
COPY --from=builder /workspace/target/*.jar app.jar
EXPOSE 8080
CMD ["java", "-jar", "app.jar"]

RUN groupadd -r appuser && useradd --no-log-init -r -g appuser appuser
USER appuser
