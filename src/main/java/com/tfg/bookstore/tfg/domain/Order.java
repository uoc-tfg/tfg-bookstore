package com.tfg.bookstore.tfg.domain;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data //getters y setters
@ToString
public class Order {

    private Integer id;
    private LocalDateTime dateCreated;
    private List<OrderBook> orderBookList;
    private User user;

    public Order() {
        this.orderBookList = new ArrayList<>();
    }

    public void addOrdersBook(List<OrderBook> orderBookList){
        this.setOrderBookList(orderBookList);
    }

    //calculamos el total de la orden de compra, productos x cantidad
    public BigDecimal getTotalOrderPrice(){
        return this.getOrderBookList().stream().map(
                b->b.getTotalPrice()
        ).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

}
