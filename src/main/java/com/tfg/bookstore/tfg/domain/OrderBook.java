package com.tfg.bookstore.tfg.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

@Data //getters y setters
@AllArgsConstructor //constructor con todos los parametros
@ToString
public class OrderBook {

    private Book book;
    private Integer quantity;
    private Order order;

    public BigDecimal getTotalPrice(){
        return this.book.getPrice().multiply(BigDecimal.valueOf(quantity));
    }

}
