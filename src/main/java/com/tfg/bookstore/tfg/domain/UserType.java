package com.tfg.bookstore.tfg.domain;

public enum UserType {

    ADMIN, USER
}
