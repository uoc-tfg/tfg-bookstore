package com.tfg.bookstore.tfg.domain;

import lombok.Data;
import lombok.ToString;

import java.time.LocalDateTime;

@Data //getters y setters
@ToString
public class Stock {

    private Integer id;
    private LocalDateTime dateCreated; //fecha de entrada de las unidades
    private Integer unitIn; //unidades de entrada
    private Integer unitOut; //unidades de las compras/salidas
    private String description;
    private Integer balance; //unidades restantes al total - unidades compradas
    private Book book;
}
