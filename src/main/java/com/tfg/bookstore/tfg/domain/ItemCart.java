package com.tfg.bookstore.tfg.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

@Data //getters y setters
@AllArgsConstructor //constructor con todos los parametros
@ToString
public class ItemCart {

    //Los atributos hacen referencia a la entidad Book
    private Integer idBook;
    private String nameBook;
    private Integer quantity;
    private BigDecimal price;

    //metodo para calcular el precio total, de cada producto del carrito
    public BigDecimal getTotalPriceItem(){
        return this.price.multiply(BigDecimal.valueOf(quantity));
    }
}
