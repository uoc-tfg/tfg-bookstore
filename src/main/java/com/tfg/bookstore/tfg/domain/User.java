package com.tfg.bookstore.tfg.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

//Utilizamos las inyecciones Lombok para tener un código mucho mas limpio y ordenado
@Data //getters y setters
@AllArgsConstructor //constructor con todos los parametros
@NoArgsConstructor //constructor vacío
@ToString
public class User {

    private Integer id;
    private String username;
    private String name;
    private String lastName;
    private String email;
    private String address;
    private String phoneNumber;
    private String password;
    private UserType userType;
    private LocalDateTime dateCreated;
}
