package com.tfg.bookstore.tfg.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

//Utilizamos las inyecciones Lombok para tener un código mucho mas limpio y ordenado
@Data //getters y setters
@AllArgsConstructor //constructor con todos los parametros
@ToString
public class Book {

    private Integer id;
    private String code;
    private String name;
    private String description;
    private String image;
    private BigDecimal price;
    private LocalDateTime dateBookCreated;
    private LocalDateTime dateBookUpdated;
    //Se añaden 2 nuevos campos {autor y categoria}
    private String author;
    private String category;

    private User user;


    //El constructor generará un código de forma aleatoria cada vez que se cree un nuevo libro
    public Book(){
        this.setCode(UUID.randomUUID().toString());
    }
}
