package com.tfg.bookstore.tfg.application.repository;

import com.tfg.bookstore.tfg.domain.Order;
import com.tfg.bookstore.tfg.domain.OrderBook;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderBookRepository {
    public OrderBook create (OrderBook orderBook);
    public Iterable<OrderBook> getOrdersBooks();
    public List<OrderBook> getOrdersBookByOrder(Order order);

}
