package com.tfg.bookstore.tfg.application.service;

import com.lowagie.text.*;
import com.lowagie.text.pdf.*;
import com.tfg.bookstore.tfg.domain.Order;
import com.tfg.bookstore.tfg.domain.OrderBook;
import java.io.OutputStream;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import lombok.extern.slf4j.Slf4j;


@Slf4j
public class InvoiceGenerator {

    public static void generateInvoice(Order order, OutputStream outputStream) throws DocumentException {

            log.info("Orden dentro de generateInvoice: {}", order.getId());

            Document document = new Document(PageSize.A4);
            PdfWriter writer = PdfWriter.getInstance(document, outputStream);
            document.open();

            Font fontTitle = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, Font.BOLD);
            Font fontContent = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, CMYKColor.BLACK);

            Paragraph paragraph1 = new Paragraph("Factura " + order.getId(), fontTitle);
            paragraph1.setAlignment(Paragraph.ALIGN_RIGHT);
            document.add(paragraph1);

            document.add(Chunk.NEWLINE);

            Paragraph paragraph2 = new Paragraph("Datos de facturación: ", fontTitle);
            paragraph2.setAlignment(Paragraph.ALIGN_LEFT);
            document.add(paragraph2);

            document.add(Chunk.NEWLINE);

            Paragraph paragraph6 = new Paragraph("Cliente: "+ order.getUser().getName() + " " + order.getUser().getLastName(), fontContent);
            paragraph6.setAlignment(Paragraph.ALIGN_LEFT);
            document.add(paragraph6);

            Paragraph paragraph7 = new Paragraph("Dirección: " + order.getUser().getAddress(), fontContent);
            paragraph7.setAlignment(Paragraph.ALIGN_LEFT);
            document.add(paragraph7);

            Paragraph paragraph8 = new Paragraph("Télefono: " + order.getUser().getPhoneNumber(), fontContent);
            paragraph8.setAlignment(Paragraph.ALIGN_LEFT);
            document.add(paragraph8);

            Paragraph paragraph9 = new Paragraph("Correo electrónico: " + order.getUser().getEmail(), fontContent);
            paragraph9.setAlignment(Paragraph.ALIGN_LEFT);
            document.add(paragraph9);

            document.add(Chunk.NEWLINE);

            Paragraph paragraph5 = new Paragraph("Información del pedido:", fontTitle);
            paragraph5.setAlignment(Paragraph.ALIGN_LEFT);
            document.add(paragraph5);
            document.add(Chunk.NEWLINE);

            Paragraph paragraph3 = new Paragraph("Fecha del pedido: " + order.getDateCreated().format(DateTimeFormatter.ofPattern("dd.MM.YYYY")), fontContent);
            paragraph3.setAlignment(Paragraph.ALIGN_LEFT);
            document.add(paragraph3);

            Paragraph paragraph4 = new Paragraph("Número del pedido: " + UUID.randomUUID(), fontContent);
            paragraph4.setAlignment(Paragraph.ALIGN_LEFT);
            document.add(paragraph4);

            document.add(Chunk.NEWLINE);


            PdfPTable table = new PdfPTable(6);
            table.setWidthPercentage(100f);
            table.setSpacingBefore(10);

            Paragraph paragraph10 = new Paragraph("Desglose del pedido:", fontTitle);
            paragraph10.setAlignment(Paragraph.ALIGN_LEFT);
            document.add(paragraph10);
            document.add(Chunk.NEWLINE);

            PdfPCell cell = new PdfPCell();

            //cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            cell.setBackgroundColor(CMYKColor.LIGHT_GRAY);
            cell.setPadding(5);

            cell.setPhrase(new Phrase("Título del libro", fontTitle));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Código del libro", fontTitle));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Autor del libro", fontTitle));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Categoría del libro", fontTitle));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Unidades", fontTitle));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Precio", fontTitle));
            table.addCell(cell);

            log.info("Lista de libros de la orden:", order.getOrderBookList());

            for (OrderBook orderBook : order.getOrderBookList()) {
                    log.info("Book Name: " + orderBook.getBook().getName());
                    log.info("Quantity: " + orderBook.getQuantity());
                    log.info("Total Price: " + orderBook.getTotalPrice());

                    PdfPCell titleCell = new PdfPCell(new Phrase(orderBook.getBook().getName(), fontContent));
                    PdfPCell codeCell = new PdfPCell(new Phrase(orderBook.getBook().getCode(), fontContent));
                    PdfPCell authorCell = new PdfPCell(new Phrase(orderBook.getBook().getAuthor(), fontContent));
                    PdfPCell categoryCell = new PdfPCell(new Phrase(orderBook.getBook().getCategory(), fontContent));
                    PdfPCell quantityCell = new PdfPCell(new Phrase(String.valueOf(orderBook.getQuantity()), fontContent));
                    PdfPCell priceCell = new PdfPCell(new Phrase(orderBook.getTotalPrice().toString() + " € ", fontContent));

                    table.addCell(titleCell);
                    table.addCell(codeCell);
                    table.addCell(authorCell);
                    table.addCell(categoryCell);
                    table.addCell(quantityCell);
                    table.addCell(priceCell);
            }

            document.add(table);

            document.add(Chunk.NEWLINE);

            Paragraph totalParagraph = new Paragraph("Importe total: " + order.getTotalOrderPrice().toString() + " € ");
            totalParagraph.setAlignment(Paragraph.ALIGN_RIGHT);
            document.add(totalParagraph);

            document.add(Chunk.NEWLINE);

            Paragraph paragraph11 = new Paragraph("Gracias por comprar en Service Remote Book ", fontTitle);
            paragraph11.setAlignment(Paragraph.ALIGN_JUSTIFIED_ALL);
            document.add(paragraph11);
            document.add(Chunk.NEWLINE);

            document.close();
            writer.close();
    }
}
