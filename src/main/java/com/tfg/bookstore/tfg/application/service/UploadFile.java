package com.tfg.bookstore.tfg.application.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

public class UploadFile {
    //indicamos la carpeta destino de las imagenes
    private final String FOLDER = "uploads";

    public String upload(MultipartFile multipartFile) throws IOException {

        //identificador UUID de la imagen
        String identifier = null;

        //revisamos si es distinto de nulo o vacio la imagen pasada como parametro
        if (multipartFile != null && !multipartFile.isEmpty()){
            byte [] bytes = multipartFile.getBytes();

            Path folderPath = Paths.get(FOLDER);
            //Si la carpeta uploads no existe, la creamos y guardamos la imagen
            if (!Files.exists(folderPath)) {
                Files.createDirectory(folderPath);
            }
            //Si no, generamos de forma random un UUID a la imagen y la guardamos
            identifier = String.valueOf(UUID.randomUUID());
            Path filePath = folderPath.resolve(identifier);
            Files.write(filePath ,bytes);
        }

        return identifier;
    }

    public void delete(String nameFile){
        File file = new File(FOLDER + nameFile);
        file.delete();
    }

}
