package com.tfg.bookstore.tfg.application.repository;

import com.tfg.bookstore.tfg.domain.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository {
    public User createUser(User user);
    public User findByEmail(String email);
    public User findById(Integer id);

}
