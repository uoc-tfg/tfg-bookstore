package com.tfg.bookstore.tfg.application.service;

import jakarta.servlet.http.HttpSession;

public class LogoutService {

    public LogoutService() {
    }

    public void logout(HttpSession httpSession){
        httpSession.removeAttribute("idUser");
    }
}
