package com.tfg.bookstore.tfg.application.repository;

import com.tfg.bookstore.tfg.domain.Order;
import com.tfg.bookstore.tfg.domain.User;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository {
    Order createOrder(Order order);
    Iterable<Order> getOrders();
    Iterable<Order> getOrdersByUser(User user);
    Order getOrderById(Integer id);
}
