package com.tfg.bookstore.tfg.application.repository;

import com.tfg.bookstore.tfg.domain.Book;
import com.tfg.bookstore.tfg.domain.User;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository {
    Iterable<Book> getBooks();
    Iterable<Book> getBooksByUser(User user);
    Book getBookById(Integer id);
    Book saveBook(Book book);
    void deleteBookById(Integer id);
    Iterable<Book> searchBookByName(String name);
    Iterable<Book> searchBookByFilters(String search);

}
