package com.tfg.bookstore.tfg.application.service;

import com.tfg.bookstore.tfg.application.repository.StockRepository;
import com.tfg.bookstore.tfg.domain.Book;
import com.tfg.bookstore.tfg.domain.Stock;

import java.util.List;

public class StockService {
    private final StockRepository stockRepository;

    public StockService(StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }

    public Stock saveStock(Stock stock){
        return stockRepository.saveStock(stock);
    }

    public List<Stock> getStockByBook(Book book){
        return stockRepository.getStockByBook(book);
    }

}
