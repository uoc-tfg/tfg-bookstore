package com.tfg.bookstore.tfg.application.service;

import com.tfg.bookstore.tfg.domain.ItemCart;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CartService {

    private List<ItemCart> itemCarts; //lista de libros en el carrito
    private HashMap<Integer, ItemCart> itemCartHashMap; //evitamos duplicados, ya que el listado se guardará por ID - Book

    public CartService() {
        this.itemCartHashMap = new HashMap<>();
        this.itemCarts = new ArrayList<>();
    }

    //metodo para añadir los libros al carrito de la compra
    public void addItemCart(Integer quantity, Integer idBook, String nameBook, BigDecimal price){
        ItemCart itemCart = new ItemCart(idBook, nameBook, quantity, price);
        itemCartHashMap.put(itemCart.getIdBook(), itemCart);
        fillList();
    }

    //metodo para calcular el total del carrito de la compra
    public  BigDecimal getTotalCart(){
        BigDecimal total = BigDecimal.ZERO;
        for (ItemCart itemCart : itemCarts){
            total = total.add(itemCart.getTotalPriceItem());
        }
        return total;
    }

    //metodo para eliminar un libro en concreto del carrito de la compra
    public void removeItemCart(Integer idBook){
        itemCartHashMap.remove(idBook);
        fillList();
    }

    //metodo para eliminar todos los libros del carrito de la compra
    public void removeAllItemsCart(){
        itemCartHashMap.clear();;
        itemCarts.clear();
    }

    private void fillList(){
        itemCarts.clear();
        itemCartHashMap.forEach(
                (integer, itemCart)-> itemCarts.add(itemCart)

        );
    }

    //metodo para comprobar por consola que se están añadiendo los libros al carrito de la compra
    public List<ItemCart> getItemCarts(){
        return itemCarts;
    }

}
