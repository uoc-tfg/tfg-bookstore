package com.tfg.bookstore.tfg.application.service;

import com.tfg.bookstore.tfg.application.repository.OrderRepository;
import com.tfg.bookstore.tfg.domain.Order;
import com.tfg.bookstore.tfg.domain.User;

public class OrderService {
    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public Order createOrder(Order order){
        return  orderRepository.createOrder(order);
    }

    public Iterable<Order> getOrders(){
        return  orderRepository.getOrders();
    }

    public Iterable<Order> getOrdersByUser(User user){
        return orderRepository.getOrdersByUser(user);
    }

    public Order getOrderById(Integer Id){
        return orderRepository.getOrderById(Id);
    }
}
