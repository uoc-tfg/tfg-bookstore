package com.tfg.bookstore.tfg.application.repository;

import com.tfg.bookstore.tfg.domain.Book;
import com.tfg.bookstore.tfg.domain.Stock;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StockRepository {
    Stock saveStock(Stock stock);
    List<Stock> getStockByBook(Book book);
}
