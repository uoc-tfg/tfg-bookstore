package com.tfg.bookstore.tfg.application.service;

import com.tfg.bookstore.tfg.application.repository.OrderBookRepository;
import com.tfg.bookstore.tfg.domain.Order;
import com.tfg.bookstore.tfg.domain.OrderBook;

import java.util.List;

public class OrderBookService {
    private final OrderBookRepository orderBookRepository;

    public OrderBookService(OrderBookRepository orderBookRepository) {
        this.orderBookRepository = orderBookRepository;
    }
    public OrderBook create(OrderBook orderBook){
        return this.orderBookRepository.create(orderBook);
    }

    public Iterable<OrderBook> getOrderBook(){
        return this.orderBookRepository.getOrdersBooks();
    }

    public List<OrderBook> getOrderBooksByOrder(Order order){
        return orderBookRepository.getOrdersBookByOrder(order);
    }
}
