package com.tfg.bookstore.tfg.application.service;


import com.tfg.bookstore.tfg.application.repository.BookRepository;
import com.tfg.bookstore.tfg.application.repository.UserRepository;
import com.tfg.bookstore.tfg.domain.Book;
import com.tfg.bookstore.tfg.domain.User;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;

@Slf4j
public class BookService {

    //Al ponerle el modificador final, le indicamos que inyecte esta clase mediante constructor
    private final BookRepository bookRepository;
    private final UserRepository userRepository;
    private final UploadFile uploadFile;

    public BookService(BookRepository bookRepository, UserRepository userRepository, UploadFile uploadFile) {
        this.bookRepository = bookRepository;
        this.userRepository = userRepository;
        this.uploadFile = uploadFile;
    }

    public Iterable<Book> getBooks(){
        return  bookRepository.getBooks();
    }

    public Iterable<Book> getBooksByFilter(String search){
        return bookRepository.searchBookByFilters(search);
    }

    public Iterable<Book> getBooksByUser(User user){
        return bookRepository.getBooksByUser(user);
    }

    public Book getBookById(Integer id){
        return  bookRepository.getBookById(id);
    }

    public User getUserById(Integer id) { return userRepository.findById(id); }

    public Book saveBook(Book book, MultipartFile multipartFile, HttpSession httpSession ) throws IOException{

        String imgIdentifier = uploadFile.upload(multipartFile);

        //Guardamos la imagen si se considera el alta de un libro nuevo
        if (book.getId()==null) {
            User user = getUserById(Integer.parseInt(httpSession.getAttribute("idUser").toString()));
            book.setDateBookCreated(LocalDateTime.now());
            book.setDateBookUpdated(LocalDateTime.now());
            book.setUser(user);
            log.info("user", user.getId());
            book.setImage(imgIdentifier);
            return bookRepository.saveBook(book);
            //Guardamos la imagen, cuando se considera una actualización de un libro
        }else{
            Book bookDB = bookRepository.getBookById(book.getId());
            book.setImage(bookDB.getImage());
            if (imgIdentifier != null) {
                book.setImage(imgIdentifier);
            }

            book.setCode(bookDB.getCode());
            book.setUser(bookDB.getUser());
            book.setDateBookCreated(bookDB.getDateBookCreated());
            book.setDateBookUpdated(LocalDateTime.now());
            return bookRepository.saveBook(book);
        }

    }

    public void deleteBookById(Integer id){
        bookRepository.deleteBookById(id);
    }



}
