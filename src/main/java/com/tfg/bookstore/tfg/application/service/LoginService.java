package com.tfg.bookstore.tfg.application.service;

import com.tfg.bookstore.tfg.domain.User;
import com.tfg.bookstore.tfg.domain.UserType;

public class LoginService {
    private final UserService userService;

    public LoginService(UserService userService) {
        this.userService = userService;
    }

    /*
     * metodo para comprobar si existe el usuario por el email
     *
     *   1 - Si existe --> devuelve true --> hace login
     *   2 - Si no existe --> devuelve false --> no hace login
     *
     */

    //retorna true si encuentra el user
    public boolean existUser(String email){
        try {
            User user = this.userService.findByEmail(email);
        }catch(Exception e){
            return false;
        }
        return true;
    }

    //obtenemos el id del usuario
    public Integer getUserId(String email){
        try{
            return this.userService.findByEmail(email).getId();
        }catch (Exception e){
            return 0;
        }
    }

    //obtener tipo de usuario
    public UserType getUserType(String email){
        return this.userService.findByEmail(email).getUserType();
    }

    //obtenemos el user por email
    public User getUser(String email){
        try{
            return this.userService.findByEmail(email);
        }catch (Exception e){
            return new User();
        }
    }

    //obtenemos el user por id
    public User getUser(Integer id){
        try{
            return this.userService.findById(id);
        }catch (Exception e){
            return new User();
        }
    }

}
