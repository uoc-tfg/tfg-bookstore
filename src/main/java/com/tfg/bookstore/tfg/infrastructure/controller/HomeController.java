package com.tfg.bookstore.tfg.infrastructure.controller;

import com.tfg.bookstore.tfg.application.service.BookService;
import com.tfg.bookstore.tfg.application.service.StockService;
import com.tfg.bookstore.tfg.domain.Book;
import com.tfg.bookstore.tfg.domain.Stock;
import com.tfg.bookstore.tfg.domain.UserType;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/")
@Slf4j
public class HomeController {
    private final BookService bookService;
    private final StockService stockService;

    public HomeController(BookService bookService, StockService stockService) {
        this.bookService = bookService;
        this.stockService = stockService;
    }

    @GetMapping
    public String home(@RequestParam (required = false, value = "search") String search, Model model, HttpSession httpSession){
        if (search == null || search.trim().isEmpty()) {
            model.addAttribute("books", bookService.getBooks());
        }
        else {
            model.addAttribute("search", search);
            model.addAttribute("books", bookService.getBooksByFilter(search));
        }

        try {
            model.addAttribute("id", httpSession.getAttribute("idUser").toString());
        }catch (Exception e){

        }
        return "home";
    }

    @GetMapping("/book-detail/{id}")
    public String bookDetail(@PathVariable Integer id, Model model, HttpSession httpSession) {
        Book book = bookService.getBookById(id);
        model.addAttribute("book", book);

        List<Stock> stocks = stockService.getStockByBook(book);
        Integer lastBalance = stocks.isEmpty() ? 0 : stocks.get(stocks.size() - 1).getBalance();//recuperamos las unidades disponibles del libro a mostrar en el detalle
        model.addAttribute("stock", lastBalance);


        if (httpSession.getAttribute("idUser") != null) {
            model.addAttribute("id", httpSession.getAttribute("idUser").toString());
        }
        else {
            model.addAttribute("id", null);
        }

        Object userType = httpSession.getAttribute("userType");
        log.info("UserType: {}", userType);
        if(userType != null && userType.equals(UserType.ADMIN)){
            return "admin/book_inventory/book_detail";
        }

        //Hacemos una comprobación del stock del libro
        if (lastBalance > 0) { //si tiene stock mostramos la vista con el stock y el botón de añadir al carrito
            return "user/books/book_detail";
        } else{ //Si no tiene stock, mostramos la vista sin la opción de añadir el carrito y un mensaje de fuera de stock temporalmente.
            return "user/books/book_detail_no_stock";
        }
    }
}
