package com.tfg.bookstore.tfg.infrastructure.adapter;

import com.tfg.bookstore.tfg.application.repository.BookRepository;
import com.tfg.bookstore.tfg.domain.Book;
import com.tfg.bookstore.tfg.domain.User;
import com.tfg.bookstore.tfg.infrastructure.entity.BookEntity;
import com.tfg.bookstore.tfg.infrastructure.mapper.BookMapper;
import com.tfg.bookstore.tfg.infrastructure.mapper.UserMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BookRepositoryImpl implements BookRepository {
    private final BookCrudRepository bookCrudRepository;
    private final BookMapper bookMapper;
    private final UserMapper userMapper;

    public BookRepositoryImpl(BookCrudRepository bookCrudRepository, BookMapper bookMapper, UserMapper userMapper) {
        this.bookCrudRepository = bookCrudRepository;
        this.bookMapper = bookMapper;
        this.userMapper = userMapper;
    }

    @Override
    public Iterable<Book> getBooks() {
        return bookMapper.toBooks(bookCrudRepository.findAll());
    }

    @Override
    public Iterable<Book> getBooksByUser(User user) {
        return bookMapper.toBooks(bookCrudRepository.findByUserEntity(userMapper.toUserEntity(user)) );
    }

    @Override
    public Book getBookById(Integer id) {
        return bookMapper.toBook(bookCrudRepository.findById(id).get());
    }

    @Override
    public Book saveBook(Book book) {
        return bookMapper.toBook(bookCrudRepository.save(bookMapper.toBookEntity(book) ) );
    }

    @Override
    public void deleteBookById(Integer id) {bookCrudRepository.deleteById(id);
    }

    @Override
    public Iterable<Book> searchBookByName(String name) {
        List<BookEntity> bookEntities = bookCrudRepository.findByNameLike(name);
        return bookMapper.toBooks(bookEntities);
    }

    @Override
    public Iterable<Book> searchBookByFilters(String search) {
        List<BookEntity> bookEntities = bookCrudRepository.findByNameContainingIgnoreCaseOrAuthorContainingIgnoreCaseOrCategoryContainingIgnoreCase(search, search, search);
        return bookMapper.toBooks(bookEntities);
    }
}
