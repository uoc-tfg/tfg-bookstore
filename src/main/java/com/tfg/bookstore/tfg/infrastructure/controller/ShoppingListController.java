package com.tfg.bookstore.tfg.infrastructure.controller;

import com.tfg.bookstore.tfg.application.service.OrderBookService;
import com.tfg.bookstore.tfg.application.service.OrderService;
import com.tfg.bookstore.tfg.application.service.UserService;
import com.tfg.bookstore.tfg.domain.Order;
import com.tfg.bookstore.tfg.domain.OrderBook;
import com.tfg.bookstore.tfg.domain.User;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/user/cart/shopping")
@Slf4j
public class ShoppingListController {
    private final OrderService orderService;
    private final UserService userService;
    private final OrderBookService orderBookService;

    public ShoppingListController(OrderService orderService, UserService userService, OrderBookService orderBookService) {
        this.orderService = orderService;
        this.userService = userService;
        this.orderBookService = orderBookService;
    }

    @GetMapping
    public String showShoppingList(Model model, HttpSession httpSession){
        List<Order> newListOrder = new ArrayList<>();
        User user = userService.findById(Integer.parseInt(httpSession.getAttribute("idUser").toString()));

        Iterable<Order> orders = orderService.getOrdersByUser(user);
        for (Order order: orders) {
            newListOrder.add(getOrdersBooks(order));
        }
        model.addAttribute("id", Integer.parseInt(httpSession.getAttribute("idUser").toString()));
        model.addAttribute("orders", newListOrder);

        return "user/orders/shoppinglist";
    }

    private Order getOrdersBooks(Order order){
        Iterable<OrderBook> orderBooks = orderBookService.getOrderBooksByOrder(order);
        order.addOrdersBook((List<OrderBook>) orderBooks);
        return order;
    }
}
