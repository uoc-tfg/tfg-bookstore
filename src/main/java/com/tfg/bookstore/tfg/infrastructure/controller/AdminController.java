package com.tfg.bookstore.tfg.infrastructure.controller;

import com.tfg.bookstore.tfg.application.service.BookService;
import com.tfg.bookstore.tfg.domain.Book;
import com.tfg.bookstore.tfg.domain.Stock;
import com.tfg.bookstore.tfg.domain.User;
import com.tfg.bookstore.tfg.domain.UserType;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/admin")
@Slf4j
public class AdminController {
    private final BookService bookService;

    public AdminController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping
    public String home(Model model){
        User user = new User();
        user.setId(1);
        //user.setUserType(UserType.ADMIN);
        Iterable<Book> Books = bookService.getBooksByUser(user);
        model.addAttribute("books", Books);
        return "admin/home_administrator";
    }

}
