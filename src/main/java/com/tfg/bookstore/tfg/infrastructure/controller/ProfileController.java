package com.tfg.bookstore.tfg.infrastructure.controller;

import com.tfg.bookstore.tfg.application.service.UserService;
import com.tfg.bookstore.tfg.domain.User;
import com.tfg.bookstore.tfg.domain.UserType;
import com.tfg.bookstore.tfg.infrastructure.dto.UserDto;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@Controller
@RequestMapping("/profile")
@Slf4j
public class ProfileController {

    private final UserService userService;

    public ProfileController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/show-profile")
    public String getUserProfile(Model model, HttpSession httpSession){
        log.info("id user desde la variable de session: {}",httpSession.getAttribute("idUser").toString());
        User user = userService.findById(Integer.parseInt(httpSession.getAttribute("idUser").toString()));
        model.addAttribute("user", user);
        model.addAttribute("id", httpSession.getAttribute("idUser").toString());
        return "user/profile/ver_perfil";
    }

    @GetMapping("/update-profile/{id}")
    public String updateProfile(@PathVariable(value = "id") Integer id, Model model, HttpSession httpSession){
        User existingUser = userService.findById(Integer.parseInt(httpSession.getAttribute("idUser").toString()));

        model.addAttribute("user", existingUser);
        model.addAttribute("id", httpSession.getAttribute("idUser").toString());
        return "user/profile/update_profile";
    }

    @PostMapping("/save-profile")
    public String saveProfile(@ModelAttribute("user") User user, HttpSession httpSession){
        User existingUser = userService.findById(Integer.parseInt(httpSession.getAttribute("idUser").toString()));

        // Actualizar solo los campos necesarios
        existingUser.setName(user.getName());
        existingUser.setLastName(user.getLastName());
        existingUser.setEmail(user.getEmail());
        existingUser.setAddress(user.getAddress());
        existingUser.setPhoneNumber(user.getPhoneNumber());
        existingUser.setUserType(UserType.USER);
        existingUser.setUsername(user.getEmail());

        userService.createUser(existingUser);
        return "redirect:/profile/show-profile";
    }





}
