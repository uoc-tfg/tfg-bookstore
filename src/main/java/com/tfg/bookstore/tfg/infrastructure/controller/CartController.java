package com.tfg.bookstore.tfg.infrastructure.controller;

import com.tfg.bookstore.tfg.application.service.CartService;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@Controller
@RequestMapping("/user/cart")
@Slf4j
public class CartController {
   private final CartService cartService;

    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    @PostMapping("/add-bookCart")
    public String addBookCart(@RequestParam Integer quantity, @RequestParam Integer idBook, @RequestParam String nameBook, @RequestParam BigDecimal price){
        cartService.addItemCart(quantity, idBook, nameBook, price);
        showCart();
        return "redirect:/";
    }

    private void showCart() {
        cartService.getItemCarts().forEach(
                itemCart -> log.info("Item cart: {}", itemCart)
        );
    }

    @GetMapping("/getShoppingCart")
    public String getCart(Model model, HttpSession httpSession){
        //log.info("id user desde la variable de session desde getCart: {}",httpSession.getAttribute("idUser").toString());
        showCart();
        model.addAttribute("cart", cartService.getItemCarts());
        model.addAttribute("total",cartService.getTotalCart());
        model.addAttribute("id", httpSession.getAttribute("idUser").toString());
        //return "user/shoppingCart/cart";
        return "user/cart/getShoppingCart";
    }

    @GetMapping("/delete-item-cart/{id}")
    public String deleteItemCart(@PathVariable Integer id){
        cartService.removeItemCart(id);
        return "redirect:/user/cart/getShoppingCart";
    }

}
