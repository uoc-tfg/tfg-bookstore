package com.tfg.bookstore.tfg.infrastructure.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //addResourceHandler --> la url para buscar la imagen
        //addResourceLocation --> irá a buscar la imagen a la carpeta uploads del docker
        registry.addResourceHandler("/images/**").addResourceLocations("file:uploads/");
    }
}
