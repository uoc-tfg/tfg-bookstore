package com.tfg.bookstore.tfg.infrastructure.entity;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "ordersBooks")
public class OrderBookEntity {

    @EmbeddedId
    private OrderBookPK orderBookPK;
    private Integer quantity;
}
