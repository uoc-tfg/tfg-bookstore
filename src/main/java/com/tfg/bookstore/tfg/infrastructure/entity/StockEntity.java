package com.tfg.bookstore.tfg.infrastructure.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.time.LocalDateTime;

@Entity
@Table(name = "stock")
@NoArgsConstructor
@Data
public class StockEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//el campo id se va a generar de forma automatica y de forma incremental en la BD
    private Integer id;
    private LocalDateTime dateCreated; //fecha de entrada de las unidades
    private Integer unitIn; //unidades de entrada
    private Integer unitOut; //unidades de las compras/salidas
    private String description;
    private Integer balance; //unidades restantes al total - unidades compradas

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)//Cuando se borre un libro, se borrará automaticamente todo su stock
    private BookEntity bookEntity;
}
