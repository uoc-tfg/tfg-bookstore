package com.tfg.bookstore.tfg.infrastructure.controller;

import com.lowagie.text.DocumentException;
import com.tfg.bookstore.tfg.application.service.InvoiceGenerator;
import com.tfg.bookstore.tfg.application.service.OrderService;
import com.tfg.bookstore.tfg.domain.Order;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;


@Controller
@RequestMapping("/user/generate-invoice")
@Slf4j
public class InvoiceController {

    private final OrderService orderService;

    public InvoiceController(OrderService orderService) {
        this.orderService = orderService;
    }

    /*@PostMapping("/generate-invoice")
    public String generateInvoice(@RequestParam("orderId") Integer id) throws DocumentException {

        //Obtenemos el ID de la orden a generar la factura
        Order order = orderService.getOrderById(id);

        // Generamos la factura en PDF
        String filename = "factura_" + order + ".pdf";
        InvoiceGenerator.generateInvoice(order, filename);

        return "redirect:/orders/shoppinglist";
    }*/

    @GetMapping("/{id}")
    public void generateInvoice(@PathVariable Integer id, HttpServletResponse response) throws IOException {
        log.info("ID pasado como @PathVarible:", id);
        log.info("Orden recuperada de DB:", orderService.getOrderById(id));

        response.setContentType("application/pdf");
        OutputStream outputStream = response.getOutputStream();

        String headerkey = "Content-Disposition";
        String headervalue = "attachment; filename= invoice"+".pdf";

        response.setHeader(headerkey, headervalue);

        InvoiceGenerator invoiceGenerator = new InvoiceGenerator();
        invoiceGenerator.generateInvoice(orderService.getOrderById(id), outputStream);
    }

}
