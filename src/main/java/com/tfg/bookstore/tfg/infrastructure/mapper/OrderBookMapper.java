package com.tfg.bookstore.tfg.infrastructure.mapper;

import com.tfg.bookstore.tfg.domain.OrderBook;
import com.tfg.bookstore.tfg.infrastructure.entity.OrderBookEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {OrderMapper.class, BookMapper.class})
public interface OrderBookMapper {

    @Mappings({
            @Mapping(source = "orderBookPK.bookEntity", target = "book"),
            @Mapping(source = "quantity", target = "quantity"),
            @Mapping(source = "orderBookPK.orderEntity", target = "order")
    })


    OrderBook toOrderBook(OrderBookEntity orderBookEntity);
    Iterable<OrderBook> toOrderBooks(Iterable<OrderBookEntity> orderBookEntities);
    List<OrderBook> toOrderBookList(Iterable<OrderBookEntity> orderBookEntities);

    @InheritInverseConfiguration
    OrderBookEntity toOrderBookEntity(OrderBook orderBook);
}
