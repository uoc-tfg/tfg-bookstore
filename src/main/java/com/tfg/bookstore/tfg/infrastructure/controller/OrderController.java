package com.tfg.bookstore.tfg.infrastructure.controller;

import com.tfg.bookstore.tfg.application.service.*;
import com.tfg.bookstore.tfg.domain.*;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/user/order")
@Slf4j
public class OrderController {
    private final CartService cartService;
    private final UserService userService;
    private final BookService bookService;
    private final OrderService orderService;
    private final OrderBookService orderBookService;
    private final StockService stockService;
    private final ValidateStock validateStock;
    private final Integer unit = 0;

    public OrderController(CartService cartService, UserService userService, BookService bookService, OrderService orderService, OrderBookService orderBookService, StockService stockService, ValidateStock validateStock) {
        this.cartService = cartService;
        this.userService = userService;
        this.bookService = bookService;
        this.orderService = orderService;
        this.orderBookService = orderBookService;
        this.stockService = stockService;
        this.validateStock = validateStock;
    }

    @GetMapping("/order-detail")
    public String showOrderDetail(Model model, HttpSession httpSession ){
        log.info("id user desde la variable de session: {}",httpSession.getAttribute("idUser").toString());
        User user = userService.findById(Integer.parseInt(httpSession.getAttribute("idUser").toString()));
        model.addAttribute("cart", cartService.getItemCarts());
        model.addAttribute("total", cartService.getTotalCart());
        model.addAttribute("user", user);
        log.info("id user desde la variable de session: {}", httpSession.getAttribute("idUser").toString());
        model.addAttribute("id", httpSession.getAttribute("idUser").toString());
        return "user/orders/order_detail";
    }

    @GetMapping("/create-order")
    public String createOrder(RedirectAttributes attributes, HttpSession httpSession){
        log.info("create order..");
        log.info("id user desde la variable de session: {}",httpSession.getAttribute("idUser").toString());
        //mediante la sesión obtenemos el usuario de la orden
        User user = userService.findById( Integer.parseInt(httpSession.getAttribute("idUser").toString()));

        //creamos la orden
        Order order = new Order();
        order.setDateCreated(LocalDateTime.now());
        order.setUser(user);
        order = orderService.createOrder(order);

        //lista de libros de la orden
        List<OrderBook> orderBooks = new ArrayList<>();
        for (ItemCart itemCart : cartService.getItemCarts()){
            orderBooks.add( new OrderBook(bookService.getBookById(itemCart.getIdBook()), itemCart.getQuantity(), order) );
        }

        //guardamos los libros en la ordenr
        orderBooks.forEach(
                op->{
                    orderBookService.create(op);
                    Stock stock = new Stock();
                    stock.setDateCreated(LocalDateTime.now());
                    stock.setBook(op.getBook());
                    stock.setDescription("Pedido");
                    stock.setUnitIn(unit);
                    stock.setUnitOut(op.getQuantity());
                    stockService.saveStock(validateStock.calculateBalance(stock));
                }
        );

        //vacíamos el carrito
        cartService.removeAllItemsCart();
        attributes.addFlashAttribute("id", httpSession.getAttribute("idUser").toString());
        return "redirect:/";
    }



}
