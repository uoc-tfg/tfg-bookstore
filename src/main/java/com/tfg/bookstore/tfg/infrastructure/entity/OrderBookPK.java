package com.tfg.bookstore.tfg.infrastructure.entity;

import jakarta.persistence.Embeddable;
import jakarta.persistence.FetchType;
import jakarta.persistence.ManyToOne;
import lombok.Data;

@Data
@Embeddable
/*
 * Esta clase es una Primary Key compuesta, formada por:

 * OrderEntity --> Order
 * BookEntity --> Book

 * */
public class OrderBookPK {

    @ManyToOne (fetch = FetchType.LAZY)
    private OrderEntity orderEntity;

    @ManyToOne (fetch = FetchType.LAZY)
    private BookEntity bookEntity;
}
