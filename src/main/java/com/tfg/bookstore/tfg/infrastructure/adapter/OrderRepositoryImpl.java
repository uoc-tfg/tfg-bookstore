package com.tfg.bookstore.tfg.infrastructure.adapter;

import com.tfg.bookstore.tfg.application.repository.OrderRepository;
import com.tfg.bookstore.tfg.domain.Book;
import com.tfg.bookstore.tfg.domain.Order;
import com.tfg.bookstore.tfg.domain.OrderBook;
import com.tfg.bookstore.tfg.domain.User;
import com.tfg.bookstore.tfg.infrastructure.entity.OrderBookEntity;
import com.tfg.bookstore.tfg.infrastructure.entity.OrderEntity;
import com.tfg.bookstore.tfg.infrastructure.mapper.BookMapper;
import com.tfg.bookstore.tfg.infrastructure.mapper.OrderBookMapper;
import com.tfg.bookstore.tfg.infrastructure.mapper.OrderMapper;
import com.tfg.bookstore.tfg.infrastructure.mapper.UserMapper;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class OrderRepositoryImpl implements OrderRepository {
    private final OrderCrudRepository orderCrudRepository;
    private final OrderBookCrudRepository orderBookCrudRepository;
    private final OrderMapper orderMapper;
    private final BookMapper bookMapper;
    private final UserMapper userMapper;

    public OrderRepositoryImpl(OrderCrudRepository orderCrudRepository, OrderBookCrudRepository orderBookCrudRepositor, OrderMapper orderMapper, BookMapper bookMapper, UserMapper userMapper) {
        this.orderCrudRepository = orderCrudRepository;
        this.orderBookCrudRepository = orderBookCrudRepositor;
        this.orderMapper = orderMapper;
        this.bookMapper = bookMapper;
        this.userMapper = userMapper;
    }

    @Override
    public Order createOrder(Order order) {
        return this.orderMapper.toOrder( orderCrudRepository.save(orderMapper.toOrderEntity(order)));
    }

    @Override
    public Iterable<Order> getOrders() {
        return this.orderMapper.toOrders( orderCrudRepository.findAll());
    }

    @Override
    public Iterable<Order> getOrdersByUser(User user) {
        return this.orderMapper.toOrders(orderCrudRepository.findByUser(userMapper.toUserEntity(user)));
    }

    @Override
    public Order getOrderById(Integer id) {
        OrderEntity orderEntity = orderCrudRepository.findById(id).get();
        Order order = this.orderMapper.toOrder(orderEntity);

        List<OrderBookEntity> orderBookEntities = orderBookCrudRepository.findByOrderBookPKOrderEntity(orderEntity);

        List<OrderBook> orderBookList = new ArrayList<>();
        for (OrderBookEntity orderBookEntity : orderBookEntities) {
            int quantity = orderBookEntity.getQuantity();
            Book book = bookMapper.toBook(orderBookEntity.getOrderBookPK().getBookEntity());
            OrderBook orderBook = new OrderBook(book, quantity, order);
            orderBookList.add(orderBook);
        }

        order.setOrderBookList(orderBookList);
        return order;
        // return this.orderMapper.toOrder(orderCrudRepository.findById(id).get());
    }
}
