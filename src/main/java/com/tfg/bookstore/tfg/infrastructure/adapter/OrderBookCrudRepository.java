package com.tfg.bookstore.tfg.infrastructure.adapter;

import com.tfg.bookstore.tfg.infrastructure.entity.OrderBookEntity;
import com.tfg.bookstore.tfg.infrastructure.entity.OrderEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderBookCrudRepository extends CrudRepository<OrderBookEntity, Integer> {

    List<OrderBookEntity> findByOrderBookPKOrderEntity(OrderEntity orderEntity);
}
