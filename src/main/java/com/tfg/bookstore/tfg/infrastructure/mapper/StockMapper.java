package com.tfg.bookstore.tfg.infrastructure.mapper;

import com.tfg.bookstore.tfg.domain.Stock;
import com.tfg.bookstore.tfg.infrastructure.entity.StockEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {BookMapper.class})
public interface StockMapper {

    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "dateCreated", target = "dateCreated"),
            @Mapping(source = "unitIn", target = "unitIn"),
            @Mapping(source = "unitOut", target = "unitOut"),
            @Mapping(source = "description", target = "description"),
            @Mapping(source = "balance", target = "balance"),
            @Mapping(source = "bookEntity", target = "book")
    }
    )
    Stock toStock (StockEntity stockEntity);
    List<Stock> toStocks (Iterable<StockEntity> stockEntities);

    @InheritInverseConfiguration
    StockEntity toStockEntity (Stock stock);
}
