package com.tfg.bookstore.tfg.infrastructure.controller;

import com.tfg.bookstore.tfg.application.service.BookService;
import com.tfg.bookstore.tfg.domain.Book;
import com.tfg.bookstore.tfg.domain.User;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Controller
@RequestMapping("/admin/books")
@Slf4j
public class BookController {
    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/create")
    public String create(){
        return "admin/book_inventory/create_book";
    }

    @PostMapping("/save-book")
    public String saveBook(Book book, @RequestParam(value = "imgFile", required = false) MultipartFile multipartFile, HttpSession httpSession ) throws IOException {
        log.info("Nombre del libro: {}", book);
        log.info("bytes:", multipartFile.getBytes().length);
        log.info("Imagen:", multipartFile.getOriginalFilename());
        bookService.saveBook(book, multipartFile, httpSession);
        //return "admin/book_inventory/create_book";
        return "redirect:/admin";
    }

    @GetMapping("/showBook")
    public String showBook(Model model, HttpSession httpSession){
        log.info("id user desde la variable de session: {}",httpSession.getAttribute("idUser").toString());
                User user = new User();
        user.setId(Integer.parseInt(httpSession.getAttribute("idUser").toString()));
        Iterable<Book> books = bookService.getBooksByUser(user);
        model.addAttribute("books", books); //el attributeName hace referencia al nombre del objeto que enviaremos a la vista (model)
        return "admin/book_inventory/show_book";
    }

    @GetMapping("/showBook/{id}")
    public String showBookById(@PathVariable Integer id, Model model){
        Book book = bookService.getBookById(id);
        log.info("Book obtenido: {}", book);
        model.addAttribute("book",book);
        return "admin/book_inventory/show_book";
    }


    @GetMapping("/edit/{id}")
    public String editBook(@PathVariable Integer id, Model model){
        Book book = bookService.getBookById(id);
        log.info("Book obtenido: {}", book);
        model.addAttribute("book",book);
        return "admin/book_inventory/edit_book";
    }

    @GetMapping("/delete/{id}")
    public String deleteBook(@PathVariable Integer id){
        bookService.deleteBookById(id);
        return "redirect:/admin/books/showBook";
        //return "admin/book_inventory/show_book";
    }


}
