package com.tfg.bookstore.tfg.infrastructure.configuration;

import com.tfg.bookstore.tfg.application.repository.*;
import com.tfg.bookstore.tfg.application.service.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.context.WebApplicationContext;

@Configuration
public class BeanConfiguration {

        /*
        La inyección de los beans de spring boot, se puede realizar mediante anotaciones como:

            @Autowired
            @Service
            @Repository

        O directamente, lo podemos hacer como en este caso, con una clase de configuración y tener todo el código mas ordenado
     */


    @Bean
    public BookService bookService(BookRepository bookRepository, UserRepository userRepository, UploadFile uploadFile){
        return new BookService(bookRepository, userRepository, uploadFile);
    }
    @Bean
    public UploadFile uploadFile(){
        return new UploadFile();
    }

    @Bean
    public InvoiceGenerator invoiceGenerator(){
        return new InvoiceGenerator();
    }

    @Bean
    public StockService stockService(StockRepository stockRepository){
        return new StockService(stockRepository);
    }
    @Bean
    public ValidateStock validateStock(StockService stockService){
        return new ValidateStock(stockService);
    }

    @Bean
    public OrderService orderService(OrderRepository orderRepository){
        return new OrderService(orderRepository);
    }

    @Bean
    public OrderBookService orderBookService(OrderBookRepository orderBookRepository){
        return  new OrderBookService(orderBookRepository);
    }

        /*
        Los Bean en Spring Boot, por defecto emplean el patrón singleton, pero en este caso:

        Eliminamos el patrón singleton, para evitar que si hay multiples usuarios realizando compras
        estos tengan el mismo carrito y se vaya modificando por cada cambio de usuario.

        Implementamos el carrito de la compra que vaya por la sesión del usuario
    */

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public  CartService cartService(){
        return  new CartService();
    }

    @Bean
    public UserService userService(UserRepository userRepository){
        return  new UserService(userRepository);
    }

    @Bean
    public RegistrationService registrationService(UserService userService, PasswordEncoder passwordEncoder){
        return  new RegistrationService(userService, passwordEncoder);
    }

    @Bean
    public LoginService loginService(UserService userService){
        return new LoginService(userService);
    }

    @Bean
    public LogoutService logoutService(){
        return  new LogoutService();
    }

}
