package com.tfg.bookstore.tfg.infrastructure.controller;

import com.tfg.bookstore.tfg.application.service.LoginService;
import com.tfg.bookstore.tfg.domain.User;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/login")
@Slf4j
public class LoginController {
    private final LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @GetMapping
    public String login(){
        return "authentication/login";
    }

    @GetMapping("/access")
    public String access(RedirectAttributes attributes, HttpSession httpSession){
        User user = loginService.getUser( Integer.parseInt( httpSession.getAttribute("idUser").toString()));
        //Nos llevamos el ID del usuario a la vista home, y cargar un menú u otro.
        attributes.addFlashAttribute("id", httpSession.getAttribute("idUser").toString());
        //revisamos si el usuario existe
        if(loginService.existUser(user.getEmail())){
            //httpSession.setAttribute("iduser", this.loginService.getUserId(userDTO.getEmail()));
            //comprobamos el tipo de usuario
           if (loginService.getUserType(user.getEmail()).name().equals("ADMIN")){
               //si es ADMIN --> cargamos su vista
               return "redirect:/admin";
           }else{
               //si es USER --> cargamos su vista
               return "redirect:/";
           }
        }
        return "redirect:/";
    }

}
