package com.tfg.bookstore.tfg.infrastructure.adapter;

import com.tfg.bookstore.tfg.infrastructure.entity.BookEntity;
import com.tfg.bookstore.tfg.infrastructure.entity.StockEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface StockCrudRepository extends CrudRepository<StockEntity, Integer> {
    List<StockEntity> findByBookEntity(BookEntity bookEntity);
}
