package com.tfg.bookstore.tfg.infrastructure.adapter;

import com.tfg.bookstore.tfg.application.repository.StockRepository;
import com.tfg.bookstore.tfg.domain.Book;
import com.tfg.bookstore.tfg.domain.Stock;
import com.tfg.bookstore.tfg.infrastructure.mapper.BookMapper;
import com.tfg.bookstore.tfg.infrastructure.mapper.StockMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StockRepositoryImpl implements StockRepository {

    private final StockCrudRepository stockCrudRepository;
    private final StockMapper stockMapper;
    private final BookMapper bookMapper;

    public StockRepositoryImpl(StockCrudRepository stockCrudRepository, StockMapper stockMapper, BookMapper bookMapper) {
        this.stockCrudRepository = stockCrudRepository;
        this.stockMapper = stockMapper;
        this.bookMapper = bookMapper;
    }

    @Override
    public Stock saveStock(Stock stock) {
        return this.stockMapper.toStock(this.stockCrudRepository.save(stockMapper.toStockEntity(stock)));
    }

    @Override
    public List<Stock> getStockByBook(Book book) {
        return this.stockMapper.toStocks(this.stockCrudRepository.findByBookEntity(bookMapper.toBookEntity(book)));
    }
}
