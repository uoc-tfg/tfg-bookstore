package com.tfg.bookstore.tfg.infrastructure.adapter;

import com.tfg.bookstore.tfg.application.repository.OrderBookRepository;
import com.tfg.bookstore.tfg.domain.Order;
import com.tfg.bookstore.tfg.domain.OrderBook;
import com.tfg.bookstore.tfg.infrastructure.mapper.OrderBookMapper;
import com.tfg.bookstore.tfg.infrastructure.mapper.OrderMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderBookRepositoryImpl implements OrderBookRepository {
    private final OrderBookCrudRepository orderBookCrudRepository;
    private final OrderMapper orderMapper;
    private  final OrderBookMapper orderBookMapper;

    public OrderBookRepositoryImpl(OrderBookCrudRepository orderBookCrudRepository, OrderMapper orderMapper, OrderBookMapper orderBookMapper) {
        this.orderBookCrudRepository = orderBookCrudRepository;
        this.orderMapper = orderMapper;
        this.orderBookMapper = orderBookMapper;
    }

    @Override
    public OrderBook create(OrderBook orderBook) {
        return this.orderBookMapper.toOrderBook(this.orderBookCrudRepository.save(orderBookMapper.toOrderBookEntity(orderBook)));
    }

    @Override
    public Iterable<OrderBook> getOrdersBooks() {
        return this.orderBookMapper.toOrderBooks(this.orderBookCrudRepository.findAll());
    }

    @Override
    public List<OrderBook> getOrdersBookByOrder(Order order) {
        return this.orderBookMapper.toOrderBookList(orderBookCrudRepository.findByOrderBookPKOrderEntity(orderMapper.toOrderEntity(order)));
    }
}
