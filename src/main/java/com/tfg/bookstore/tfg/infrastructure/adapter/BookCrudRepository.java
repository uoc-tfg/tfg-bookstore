package com.tfg.bookstore.tfg.infrastructure.adapter;

import com.tfg.bookstore.tfg.infrastructure.entity.BookEntity;
import com.tfg.bookstore.tfg.infrastructure.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BookCrudRepository  extends CrudRepository<BookEntity, Integer> {
    Iterable<BookEntity> findByUserEntity (UserEntity userEntity);
    List<BookEntity> findByNameLike(String name);
    List<BookEntity> findByNameContainingIgnoreCaseOrAuthorContainingIgnoreCaseOrCategoryContainingIgnoreCase(String name, String author, String category);
}
