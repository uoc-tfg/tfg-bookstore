package com.tfg.bookstore.tfg.infrastructure.dto;

import com.tfg.bookstore.tfg.domain.User;
import com.tfg.bookstore.tfg.domain.UserType;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UserDto {

    //validamos los campos introducidos en la vista del registro
    private String username;
    @NotBlank(message = "El campo nombre es obligatorio")
    private String name;
    @NotBlank(message = "El campo apellido es obligatorio")
    private String lastName;
    @Email(message = "Escribe un email valido")
    private String email;
    @NotBlank(message = "El campo dirección es obligatorio")
    private String address;
    @NotBlank(message = "El campo telefono es obligatorio")
    private String phoneNumber;
    @NotBlank(message = "El campo contraseña es obligatorio")
    private String password;

    /*
     * Marcamos dos veces el getEmail(), ya que la comprobación de las credenciales del login
     * se realizará mediante el correo --> username
     */

    public User userDtoToUser(){
        return new User(null, this.getEmail(), this.getName(), this.getLastName(),
                this.getEmail(), this.getAddress(), this.getPhoneNumber(), this.getPassword(),
                UserType.USER, LocalDateTime.now());
    }
}
