package com.tfg.bookstore.tfg.infrastructure.mapper;

import com.tfg.bookstore.tfg.domain.Book;
import com.tfg.bookstore.tfg.infrastructure.entity.BookEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface BookMapper {


    @Mappings(
            {
                    @Mapping(source = "id", target = "id"),
                    @Mapping(source = "code", target = "code"),
                    @Mapping(source = "name", target = "name"),
                    @Mapping(source = "description", target = "description"),
                    @Mapping(source = "image", target = "image"),
                    @Mapping(source = "price", target = "price"),
                    @Mapping(source = "dateBookCreated", target = "dateBookCreated"),
                    @Mapping(source = "dateBookUpdated", target = "dateBookUpdated"),
                    //Se añaden 2 nuevos campos {autor y categoria}
                    @Mapping(source = "author", target = "author"),
                    @Mapping(source = "category", target = "category"),
                    @Mapping(source = "userEntity", target = "user")

            }
    )
    Book toBook(BookEntity bookEntity);

    Iterable<Book> toBooks(Iterable<BookEntity> bookEntities);

    @InheritInverseConfiguration
    BookEntity toBookEntity(Book book);
}
