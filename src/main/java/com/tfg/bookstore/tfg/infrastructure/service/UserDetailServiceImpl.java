package com.tfg.bookstore.tfg.infrastructure.service;

import com.tfg.bookstore.tfg.application.service.LoginService;
import com.tfg.bookstore.tfg.domain.User;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private LoginService loginService;

    @Autowired
    private HttpSession httpSession;

    private final Integer USER_NOT_FOUND = 0;



    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Integer idUser = this.loginService.getUserId(username);
        if(idUser != USER_NOT_FOUND){
            User user = this.loginService.getUser(username);
            httpSession.setAttribute("idUser", user.getId());
            httpSession.setAttribute("userType", user.getUserType());
            System.out.println("ID del usuario " + user.getId());
            return org.springframework.security.core.userdetails.User.builder()
                    .username(user.getUsername())
                    .password(user.getPassword())
                    .roles(user.getUserType().name()).build();
        }else{
            throw new UsernameNotFoundException("El usuario no existe");
        }

    }
}
