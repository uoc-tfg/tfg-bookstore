package com.tfg.bookstore.tfg.infrastructure.adapter;

import com.tfg.bookstore.tfg.infrastructure.entity.OrderEntity;
import com.tfg.bookstore.tfg.infrastructure.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;

public interface OrderCrudRepository extends CrudRepository<OrderEntity , Integer> {

    Iterable<OrderEntity>findByUser(UserEntity userEntity);
}
