package com.tfg.bookstore.tfg.infrastructure.mapper;

import com.tfg.bookstore.tfg.domain.User;
import com.tfg.bookstore.tfg.infrastructure.entity.UserEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface UserMapper {

    /*
        Mapeamos todo el objeto User con sus atributos, el source es el atributo de entrada (UserEntity)
        y el target es el atributo destino (User)

     */
    @Mappings(
            {
                    @Mapping(source = "id", target = "id"),
                    @Mapping(source = "username", target = "username"),
                    @Mapping(source = "name", target = "name"),
                    @Mapping(source = "lastName", target = "lastName"),
                    @Mapping(source = "email", target = "email"),
                    @Mapping(source = "address", target = "address"),
                    @Mapping(source = "phoneNumber", target = "phoneNumber"),
                    @Mapping(source = "password", target = "password"),
                    @Mapping(source = "userType", target = "userType"),
                    @Mapping(source = "dateCreated", target = "dateCreated")
            }
    )

    User toUser(UserEntity userEntity); //mapeamos de UserEntity a User, para no mezclas las tablas de la BD con las clases de Domain

    Iterable<User> toUsers(Iterable<UserEntity> userEntities);

    //Ahora realizamos la misma configuración pero a la inversa, es decir, de User a UserEntity
    @InheritInverseConfiguration
    UserEntity toUserEntity(User user);
}
