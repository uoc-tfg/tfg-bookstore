package com.tfg.bookstore.tfg.infrastructure.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

@Data //getters y setters
@Entity
@Table(name = "orders")//las tablas en postgreSQL siempre deben ir en plural
@NoArgsConstructor //constructor vacío
@AllArgsConstructor //constructor con todos los parametros
@ToString
public class OrderEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//el campo id se va a generar de forma automatica y de forma incremental en la BD
    private Integer id;
    private LocalDateTime dateCreated;
    @ManyToOne
    private UserEntity user;


}
