package com.tfg.bookstore.tfg.infrastructure.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data //getters y setters
@Entity
@Table(name = "books")//las tablas en postgreSQL siempre deben ir en plural
@NoArgsConstructor //constructor vacío
@AllArgsConstructor //constructor con todos los parametros
@ToString
public class BookEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//el campo id se va a generar de forma automatica y de forma incremental en la BD
    private Integer id;
    private String code;
    private String name;
    private String description;
    private String image;
    private BigDecimal price;
    private LocalDateTime dateBookCreated;
    private LocalDateTime dateBookUpdated;
    //Se añaden 2 nuevos campos {autor y categoria}
    private String author;
    private String category;

    @ManyToOne//creamos una relación * a 1 con UserEntity
    private UserEntity userEntity;
}
