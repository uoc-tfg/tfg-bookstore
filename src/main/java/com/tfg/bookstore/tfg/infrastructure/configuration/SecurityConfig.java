package com.tfg.bookstore.tfg.infrastructure.configuration;

import com.tfg.bookstore.tfg.infrastructure.service.LoginHandler;
import com.tfg.bookstore.tfg.infrastructure.service.UserDetailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Autowired
    private UserDetailServiceImpl userDetailService;

    @Autowired
    private LoginHandler loginHandler;

    /*
     * Método para poder realizar la autentificación del usuario
     * utilizando Spring Security --> UserDetailsServiceImpl
     */

    @Bean
    public DaoAuthenticationProvider authenticationProvider(){
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userDetailService);
        //Encriptamos la contraseña introducida en el login con la que hay guardada en la BD
        provider.setPasswordEncoder(passwordEncoder());
        return provider;
    }

    //Método para encriptar las contraseñas introducidas tanto en el registro como en el login
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    /*
     * Configuración de acceso por URL y con las credenciales comprobadas del usuario
     * damos acceso a los diferentes controladores de la aplicación
     * mediante el uso de los roles de usuario
     *
     * También indicamos a Spring Security que tenemos un login propio para comprobar las credenciales
     * y si es OK, entonces hace la redirección al método GetMapping("/access) de LoginController
     */

    /*
     * con la anotación anyRequest().permitAll()
     * indicamos que las URL de mas bajo nivel, serán visibles para los usuarios que no estén registrados
     */

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.csrf().disable().authorizeHttpRequests()
                .requestMatchers("/admin/**").hasRole("ADMIN")
                .requestMatchers("/user/**").hasRole("USER")
                .anyRequest().permitAll()
                .and()
                    .formLogin()
                    .loginPage("/login")
                    .defaultSuccessUrl("/login/access")
                .and()
                    .logout()
                    .logoutSuccessUrl("/")
                .deleteCookies("JSESSIONID");
        return httpSecurity.build();
    }


    /*
    * Realizamos la configuración del listado de peticiones HTTP que puede requerir el backend
    * para que no haya prohibición o bloqueo por parte del navegador en realizar
    * las peticiones entre el backend y frontend, y mostrar la información correctamente en las vistas    *
    */

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOriginPatterns(List.of("http://localhost:8080"));
        configuration.setAllowedMethods(List.of("GET", "POST", "OPTIONS", "DELETE", "PUT", "PATCH"));
        configuration.setAllowedHeaders(List.of("Access-Control-Allow-Origin", "X-Requested-With", "Origin", "Content-Type", "Accept", "Authorization"));
        configuration.setAllowCredentials(true);
        return new UrlBasedCorsConfigurationSource();
    }
}
