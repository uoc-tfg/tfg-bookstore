package com.tfg.bookstore.tfg.infrastructure.entity;

import com.tfg.bookstore.tfg.domain.UserType;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data //getters y setters
@Entity
@Table(name = "users")//user es palabra reserva en postgreSQL, y da error
@NoArgsConstructor //constructor vacío
@AllArgsConstructor //constructor con todos los parametros
@ToString
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//el campo id se va a generar de forma automatica y de forma incremental en la BD
    private Integer id;
    private String username;
    private String name;
    private String lastName;
    private String email;
    private String address;
    private String phoneNumber;
    private String password;
    @Enumerated(EnumType.STRING)
    private UserType userType;
    private LocalDateTime dateCreated;

}
