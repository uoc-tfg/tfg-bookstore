package com.tfg.bookstore.tfg.infrastructure.controller;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.checkout.Session;
import com.stripe.param.checkout.SessionCreateParams;
import com.tfg.bookstore.tfg.application.service.*;
import com.tfg.bookstore.tfg.domain.*;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/user/payments")
@Slf4j
public class CheckoutController {

    @Value("${STRIPE_PUBLIC_KEY}")
    private String stripePublicKey;

    @Value("${STRIPE_PRIVATE_KEY}")
    private String stripePrivateKey;

    private final CartService cartService;
    private final UserService userService;
    private final BookService bookService;
    private final OrderService orderService;
    private final OrderBookService orderBookService;
    private final StockService stockService;
    private final ValidateStock validateStock;

    public CheckoutController(CartService cartService, UserService userService, BookService bookService, OrderService orderService, OrderBookService orderBookService, StockService stockService, ValidateStock validateStock) {
        this.cartService = cartService;
        this.userService = userService;
        this.bookService = bookService;
        this.orderService = orderService;
        this.orderBookService = orderBookService;
        this.stockService = stockService;
        this.validateStock = validateStock;
    }

    @PostMapping("/create-checkout-session")
    public void createCheckoutSession(HttpServletResponse response, HttpSession httpSession) {

        //obtenemos la id de la orden por el carrito de la compra
        Order order = orderService.getOrderById(createOrder(httpSession));
        //cogemos la clave privada de Stripe --> esta en el application.properties
        Stripe.apiKey = stripePrivateKey;

        //Creamos las línas de los pedidos, para la pasarela de pago
        List<SessionCreateParams.LineItem> lineItemsStripe = new ArrayList<>();
        for (OrderBook orderBook : order.getOrderBookList()) {
            SessionCreateParams.LineItem lineItem = SessionCreateParams.LineItem.builder()
                    .setQuantity(orderBook.getQuantity().longValue())
                    .setPriceData(
                            SessionCreateParams.LineItem.PriceData.builder()
                                    .setCurrency("eur")
                                    /*
                                        se multipla por 100L, porque stripe espera un long
                                        Un ejemplo: 25,25€ --> 2525L
                                    */
                                    .setUnitAmount(orderBook.getBook().getPrice().longValue() * 100L)
                                    .setProductData(
                                            SessionCreateParams.LineItem.PriceData.ProductData.builder()
                                                    .setName(orderBook.getBook().getName())
                                                    .build()
                                    )
                                    .build()
                    )
                    .build();

            lineItemsStripe.add(lineItem);
        }

        /*
            Creamos la sesión del pago y indicamos el tipo de pago, en este pago mediante tarjeta
            también indicamos las URL de los 2 casos a considerar:
            OK --> sucess
            KO --> cancel
         */

        String YOUR_DOMAIN = "http://localhost:8080";
        SessionCreateParams params =
                SessionCreateParams.builder()
                        .setMode(SessionCreateParams.Mode.PAYMENT)
                        .setSuccessUrl(YOUR_DOMAIN + "/user/payments/success?session_id={CHECKOUT_SESSION_ID}")
                        .setCancelUrl(YOUR_DOMAIN + "/user/payments/cancel?session_id={CHECKOUT_SESSION_ID}")
                        .addAllLineItem(lineItemsStripe)
                        .addPaymentMethodType(SessionCreateParams.PaymentMethodType.CARD)
                        .putMetadata("shop_order_id", String.valueOf(order.getId()))
                        .build();

        Session session;
        try {
             session = Session.create(params);
        } catch (StripeException e) {
            log.error("Se ha producido un error en create-checkout-session", e);
            throw new RuntimeException(e);
        }

        response.setHeader("Location", session.getUrl());
        response.setStatus(HttpStatus.SEE_OTHER.value());
    }

    @GetMapping("/success")
    public String success(Model model) {
        return "user/payments/success";
    }


    @GetMapping("/cancel")
    public String cancel(Model model){
        return "user/payments/cancel";
    }

    private Integer createOrder(HttpSession httpSession) {
        log.info("create order..");
        log.info("id user desde la variable de session: {}",httpSession.getAttribute("idUser").toString());
        //mediante la sesión obtenemos el usuario de la orden
        User user = userService.findById( Integer.parseInt(httpSession.getAttribute("idUser").toString()));

        //creamos la orden
        Order order = new Order();
        order.setDateCreated(LocalDateTime.now());
        order.setUser(user);
        order = orderService.createOrder(order);

        //lista de libros de la orden
        List<OrderBook> orderBooks = new ArrayList<>();
        for (ItemCart itemCart : cartService.getItemCarts()){
            orderBooks.add( new OrderBook(bookService.getBookById(itemCart.getIdBook()), itemCart.getQuantity(), order) );
        }

        //guardamos los libros en la ordenr
        orderBooks.forEach(
                op->{
                    orderBookService.create(op);
                    Stock stock = new Stock();
                    stock.setDateCreated(LocalDateTime.now());
                    stock.setBook(op.getBook());
                    stock.setDescription("Pedido");
                    stock.setUnitIn(0);
                    stock.setUnitOut(op.getQuantity());
                    stockService.saveStock(validateStock.calculateBalance(stock));
                }
        );

        //vacíamos el carrito
        cartService.removeAllItemsCart();
        return order.getId();
    }

}

