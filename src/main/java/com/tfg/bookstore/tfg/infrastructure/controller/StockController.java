package com.tfg.bookstore.tfg.infrastructure.controller;

import com.tfg.bookstore.tfg.application.service.BookService;
import com.tfg.bookstore.tfg.application.service.StockService;
import com.tfg.bookstore.tfg.application.service.ValidateStock;
import com.tfg.bookstore.tfg.domain.Book;
import com.tfg.bookstore.tfg.domain.Stock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping("/admin/books/stock")
@Slf4j
public class StockController {

    private final StockService stockService;
    private final ValidateStock validateStock;
    private final BookService bookService;

    public StockController(StockService stockService, ValidateStock validateStock, BookService bookService) {
        this.stockService = stockService;
        this.validateStock = validateStock;
        this.bookService = bookService;
    }

    @GetMapping("/{id}")
    public String show (@PathVariable Integer id, Model model){
        List<Stock> stocks = stockService.getStockByBook(bookService.getBookById(id));
        model.addAttribute("stocks", stocks);
        model.addAttribute("idBook", id);
        return "admin/book_stock/show-stock";
    }

    @GetMapping("/create-unit-book/{id}")
    public String create(@PathVariable Integer id, Model model){
        model.addAttribute("idBook", id);
        return "admin/book_stock/add-stock";
    }

    @PostMapping("/save-unit-book")
    public String save(Stock stock, @RequestParam("idBook") Integer idBook, String description){
        log.info("idBook obtenido: {}", idBook);
        log.info("Comentario: {}", description);
        stock.setDateCreated(LocalDateTime.now());
        stock.setDescription(description);
        stock.setBook(bookService.getBookById(idBook));
        stockService.saveStock(validateStock.calculateBalance(stock));
        return "redirect:/admin/books/showBook";
    }

}
