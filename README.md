# TFG BookStore

## Información básica ##

Este es el repositorio principal del proyecto Service Remote Book (bookstore), donde se concentra la lógica de negocio del backend y frontend.

	- La parte del backend esta realizada con:
	```
	Spring Boot
	JPA
	Spring Security
	Lombok
	Mapstruc
	```
	
	- La base de datos es:
		```postgreSQL
		```
	
	- La infraestructura del frontend esta realizada con:
		````
		Thymeleaf
		 HTML
		 CSS
		 Bootstrap 4
		 Javascript
		````
	
	- También se ha realizado una implementación con Stripe, para la configuración de la pasarela de pago.
	
	- Todo el proyecto esta montado sobre Docker con su respectivo Dockerfile


## Realizar pruebas

```
cd existing_repo
git remote add origin https://gitlab.com/igranah/tfg-bookstore.git
git branch -M main
git push -uf origin main
```


## Backend ##

La aplicación consta de 2 roles:
 * UserType_ADMIN
 * UserType_USER
 
- El ADMIN es el encargado de realizar las acciones de creación y gestión, tanto del catalogo como del stock disponible
- El USER es el cliente que se registra, visualiza el catalogo, y compra los libros que mas le interesen.

## Consideraciones ##

El registro siempre hace un INSERT en la tabla User del UserType_USER, por lo que habrá que modificar 1 usuario
para que sea UserType_ADMIN y poder crear un catalogo de pruebas.


## Consideraciones previas ##

El registro siempre hace un INSERT en la tabla User del UserType_USER, por lo que habrá que modificar 1 usuario
para que sea UserType_ADMIN y poder crear un catalogo de pruebas.

En el apartado del checkout de Stripe, se utiliza su API en el entorno SandBox/TEST, por lo tanto
los datos bancarios son de test.

 * El nº de la targeta ha utilizar es: 4242 4242 4242
 * El resto de datos pueden ser inventados, pero cumpliendo con el formato correcto
